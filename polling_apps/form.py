from django import forms
from .models import *
from apps_public.models import *

class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        text = "URL Berita : "+str(obj.url)
        return text

class PollingBeritaForm(forms.Form):
	urls = {
	        'type': 'text',
	        'class': 'form-control',
	    }

	start_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll started at",
	    }


	end_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll ended at",

	    }

	Question = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Insert the question",
	}

	Answer = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Ex. option1, option2, option3, etc",
	}

	Urlberita = MyModelChoiceField(queryset=Berita.objects.all().order_by('url'), required=True,
		empty_label="Select the news url for your polling", label='URL Berita', widget=forms.Select(attrs={'class':'form-control'}))
	Startdate =  forms.DateTimeField(label='Start Date', required=True, widget=forms.DateInput(attrs=start_date))
	EndDate =  forms.DateTimeField(label='End Date', required=True, widget=forms.DateInput(attrs=end_date))
	Questions =  forms.CharField(label='Poll Questions', required=True, max_length=500, widget=forms.TextInput(attrs=Question))
	Answers =  forms.CharField(label='Options', required=True, max_length=500, widget=forms.TextInput(attrs=Answer))



class PollingForm(forms.Form):
	deskripsipolling = {
	        'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Describe your poll",
	        'size' : '10'
	    }

	Urlpolling = {
	        'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Generate url for your polling",
	    }


	start_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll started at"
	    }


	end_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll ended at"
	    }

	Question = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Insert the question",
	}

	Answer = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Ex. option1, option2, option3, etc",
	}

	Deskripsi =  forms.CharField(label='Description', required=True, max_length=500, widget=forms.TextInput(attrs=deskripsipolling))
	Url =  forms.CharField(label='Url Polling', required=True, max_length=200, widget=forms.TextInput(attrs=Urlpolling))
	Startdate =  forms.DateTimeField(label='Start Date', required=True, widget=forms.DateInput(attrs=start_date))
	EndDate =  forms.DateTimeField(label='End Date', required=True, widget=forms.DateInput(attrs=end_date))
	Questions =  forms.CharField(label='Poll Questions', required=True, max_length=500, widget=forms.TextInput(attrs=Question))
	Answers =  forms.CharField(label='Options', required=True, max_length=500, widget=forms.TextInput(attrs=Answer))
