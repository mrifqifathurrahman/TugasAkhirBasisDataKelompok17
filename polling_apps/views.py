from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .form import *
from apps_public.models import *
from django.db import connection
from django.contrib import messages
from apps_login.models import *
from apps_public.models import *
from .models import *


response = {}

def polling_page(request):
    response ['polling_berita_form'] = PollingBeritaForm
    response ['PollingForm'] = PollingForm
    pol_news = PollingBerita.objects.all()
    response['pol_news'] = pol_news

    pol_biasa = PollingBiasa.objects.all()
    response['pol_reg'] = pol_biasa
    return render(request, 'polling_page.html', response)


    #cursor = connection.cursor()
    #query = 'select id_polling from polling, polling_berita where id_polling = id'
    #cursor.execute(query)
    #pol_berita = cursor.fetchall()
    #print(pol_berita)
    #pol_news = []
    #for x in pol_berita:
    	#tmp = PollingBerita.objects.get(id_polling=x[0])
    	#pol_news.append(tmp)
    #print(pol_news)


@csrf_exempt
def createNewsPolling(request):
    form = PollingBeritaForm(data=request.POST)

    if (request.method == 'POST' and form.is_valid()):
        cursor = connection.cursor()
        query = ('select count(*) from Polling')
        cursor.execute(query)
        count = cursor.fetchone()[0] #(2,)

        id_polling = 0
        if count > 0:
            id_polling = count + 1
        else:
            id_polling = 1

        url_berita= request.POST.get('Urlberita', False)#request.POST.get['username']
        start_date = request.POST.get('Startdate', False)
        end_date = request.POST.get('EndDate', False)
        questions = request.POST.get('Questions', False)
        answers = request.POST.get('Answers', False)

        #insert new polling
        query = ("insert into Polling (id, polling_start, polling_end, total_responden) values (" + str(id_polling) + ",'"
            + str(start_date) + "','" + str(end_date) + "'," + str(0) + ");" )
        cursor.execute(query)

        #insert new polling berita
        query = "insert into Polling_berita (id_polling, url_berita) values (" + str(id_polling) + ",'" + str(url_berita) +"');"
        cursor.execute(query)

        messages.success(request, "News Polling Submitted!")
        return HttpResponseRedirect(reverse('apps-polling:polling-page'))
    else :
    	return HttpResponse("null")


@csrf_exempt
def createPolling(request):
    form = PollingForm(data=request.POST)

    if (request.method == 'POST' and form.is_valid()):
        cursor = connection.cursor()
        cursor.execute('select count(*) from Polling')
        count = cursor.fetchone()[0] #(2,)

        id_polling = 0
        if count > 0:
            id_polling = count + 1
        else:
            id_polling = 1

        descriptions = request.POST.get('Deskripsi', False)#request.POST.get['username']
        url = request.POST.get('Url', False)
        start_date = request.POST.get('Startdate', False)
        end_date = request.POST.get('EndDate', False)
        questions = request.POST.get('Questions', False)
        answers = request.POST.get('Answers', False)

        # insert new polling
        query = ("insert into Polling (id, polling_start, polling_end, total_responden) values (" + str(id_polling) + ",'"
            + str(start_date) + "','" + str(end_date) + "'," + str(0) + ");" )
        cursor.execute(query)

       	#insert new polling biasa
        query = ("insert into polling_biasa (id_polling, url, deskripsi) values (" + str(id_polling) + ",'" + str(url) + "','"
            + str(descriptions) +"');")
       	cursor.execute(query)

        messages.success(request, "Polling Submitted!")
        return HttpResponseRedirect(reverse('apps-polling:polling-page'))
    else :
    	return HttpResponse("null")
