from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', polling_page, name='polling-page'),
    url(r'^submit/$', createNewsPolling, name='create-NewsPolling'),
    url(r'^submits/$', createPolling, name='create-Polling'),
]
