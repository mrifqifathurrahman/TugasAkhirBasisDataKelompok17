from django.apps import AppConfig


class AppsPollingConfig(AppConfig):
    name = 'apps_polling'
