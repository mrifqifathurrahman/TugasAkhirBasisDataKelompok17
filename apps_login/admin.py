from django.contrib import admin
from apps_login.models import *

# Register your models here.
admin.site.register(Universitas)
admin.site.register(Narasumber)
admin.site.register(Dosen)
admin.site.register(Staf)
admin.site.register(Mahasiswa)
