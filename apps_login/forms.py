from django import forms
from .models import *

class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        text = "University #"+str(obj.id)+" - "+str(obj.website)
        return text

class LoginForm(forms.Form):
    username = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your username',
    }
    password = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Enter your password',
    }
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))

class RegisterForm(forms.Form):
    role = {
        ('', '---------------'),
        ('dosen', 'Dosen'),
        ('staf', 'Staf'),
        ('mahasiswa', 'Mahasiswa'),
    }
    status = {
        ('', '---------------'),
        ('aktif', 'Aktif'),
        ('tidak', 'Tidak Aktif'),
    }
    error_message = {
        'required': 'This field is required to fill',
    }
    username = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your username',
    }
    password = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Minimal 8 characters',
    }
    nomor_identitas = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your NIK/NPM',
        'disabled':'true'
    }
    nama = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your full name',
    }
    tempat_lahir = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your birth place',
    }
    tanggal_lahir = {
        'type': 'date',
        'class': 'form-control',
    }
    email = {
        'type': 'email',
        'class': 'form-control',
        'placeholder': 'Enter your valid email adress',
    }
    no_hp = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter a valid phone number',
    }
    jurusan = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your major. Ex: Information System, Computer Science, etc.',
        'disabled':'true'
    }
    posisi = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your position. Ex: Administrator, Human Resources, etc.',
        'disabled':'true'
    }

    jurusan = forms.CharField(label='Jurusan', required=False, max_length=50, widget=forms.TextInput(attrs=jurusan))
    posisi = forms.CharField(label='Posisi', required=False, max_length=50, widget=forms.TextInput(attrs=posisi))

    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))

    nama = forms.CharField(label='Full Name', required=True, max_length=50, widget=forms.TextInput(attrs=nama))
    email = forms.CharField(label='Email adress', required=True, max_length=50, widget=forms.TextInput(attrs=email))
    tempat = forms.CharField(label='Tempat Lahir', required=True, max_length=50, widget=forms.TextInput(attrs=tempat_lahir))
    tanggal = forms.DateTimeField(label='Birthday Date', required=True, widget=forms.DateInput(attrs=tanggal_lahir))
    no_hp = forms.CharField(label='Phone Number', max_length=50,required=False, widget=forms.TextInput(attrs=no_hp))
    nomor_identitas = forms.CharField(label='Identity Number (NPM,NIK)', required=True, max_length=20, widget=forms.TextInput(attrs=nomor_identitas))

    role = forms.ChoiceField(label='Role', choices=role, required=True, widget=forms.Select(attrs={'class':'form-control'}))
    status = forms.ChoiceField(label='Status Kemahasiswaan', required=False, choices=status, widget=forms.Select(attrs={'class':'form-control','disabled':'true'}))
    id_universitas = MyModelChoiceField(queryset=Universitas.objects.all().order_by('id'), required=True, empty_label="Select your university", label='ID Universitas', widget=forms.Select(attrs={'class':'form-control','disabled':'true'}))
    #id_universitas = forms.MyModelChoiceField(queryset=Universitas.objects.all().order_by('id'), label='ID Universitas', required=True, widget=forms.Select(attrs={'class':'form-control'}))
